"use strict";
import { t, Text } from "@mimer/text";
import Player from "./player";
import { BattelAction } from "../game/battelAction";
import Mode from "./mode";
import {Config} from "./game";

function values(o: any) {
  return Object.keys(o).map((key) => o[key]);
}

type statusType = "start" | "active" | "prolog" | "end";

/**
 * The  Game class creting a game engine for logic.
 */
export default class Battel extends Mode {
  status: statusType = "start";
  actions: BattelAction[] = [];

  constructor(players: Player[], config?:Config) {
    super(players, config);
  }


  move(command: { prompt: number }) {
    let result = t();
    switch (this.status) {
      case "start":
        this.status = "active";
        this.msgTurn = t("Battel begin!").bold.headline;
        break;
      case "active":
        this.status = "prolog";
        this.msgTurn = t(t("Turn: ", this.turn).bold.headline, t("\n").newline);
       // let action = this.players[playerId].team[0].selection()[command.prompt];
        break;
      case "prolog":
        this.status = "end";
        this.msgTurn = t("Battel end!").headline;
        break;
    }
    return "active";
  }
/*
  nextTurn() {
    let result = t();
    this.actions = [];
    result.add(super.endTurn());
    result.add(
      t(
        ...this.players["one"].team[0]
          .selection()
          .map((item: BattelAction) => item.occasions[0].effect[0].showInfo)
      ).orderedList
    ).newline;
    return result;
  }
*/
}
