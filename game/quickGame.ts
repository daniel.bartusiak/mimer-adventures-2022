"use strict";
import { t, Text } from "@mimer/text";
import Player from "./player";
import {Config} from "./game";
import Game from "./game";
import Mode from "./mode";
import Start from "./start";
import Battel from "./battel";

import Creature from "../world/creatures/creature";

type gameStatus = "start" | "friend" | "enemy" | "battel"|"next"|"end"|"error";

/**
 * To play a part of the Game quick and simpel.
 */
export default class QuickGame extends Game {
  status: gameStatus = "start";
  mode:Mode;

  constructor(players: Player[], config?:Config) {
    super(players, config);
    this.mode = new Start(players);
//  (this.mode as Select).setNameOnCreatue("Adam");
  }

  next(id: number, answer: { [key: string]: number | string }): gameStatus {
    let player = this.players[id];
    let result = t();
    player.msg = result;
        switch (this.status) {
          case "start": {
              let result = this.mode.move(0, {});
              this.status = "friend"
          }
          break;
        /*  case "friend":
            const creature: Creature = this.mode.endMode();
            creature.roll();
            this.players[0].team.push(creature);
            result.add(creature.showStats());
            result.add(t().newline);
            result.add(creature.saySalute);
            result.add(t().newline);
            this.mode = new Select();
            (this.mode as Select).setNameOnCreatue("Bertil");
            this.status = "enemy";
            break;
          case "enemy":
            const player = new Player("AI random");
            this.addPlayer(player);
            const enemy: Creature = this.mode.endMode();
            enemy.roll();
            this.players[1].team.push(enemy);
            result.add(enemy.showStats());
            result.add(t().newline);
            result.add(enemy.saySalute);
            result.add(t().newline);
            this.mode = new Battel();
            (this.mode as Battel).addPlayer("one", this.players[0]);
            (this.mode as Battel).addPlayer("two", this.players[1]);
            this.status = "battel";
            break;
          case "battel":
            result.add(t("\nEnd of battel\n").bold.red);
            this.status = "battel";
            break;*/
          default:
           result.add("error");
        }
      return "next";
    } 
  }

/*
if (this.mode) {
  if (this.status !== "battel") {
    result.add(this.mode.move(command, ""));
    result.add(t("\n"));
    result.add(this.mode.endTurn());
  } else {
    result.add(this.mode.move(command, "one"));
    result.add(t("\n"));
    result.add(this.mode.move({ prompt: 1 }, "two"));
    result.add(this.mode.endTurn());
  }
  if (!this.mode.isMode()) {

    else {
      player.msg = t("Error: No mode choosen").red;
      return "error";
    }
    */