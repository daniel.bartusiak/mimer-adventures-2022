"use strict";
import { t, Text } from "@mimer/text";
import Player from "./player";
import {Config} from "./game";

export default abstract class Mode {
  name = "";
  players: Player[];
  msgTurn: Text = t();
  turn = 0;

  constructor(players: Player[], config:Config={}) {
    this.name = config.name??"";
    this.players = players;
  }

  abstract move(playerId:number,
    command: { [key: string]: number | string }
  ): String;

  protected endTurn() {
    this.turn++;
    return this.msgTurn;
  }
}
