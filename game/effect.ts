import { Calculation, c } from "@mimer/calculation";
import { t, Text } from "@mimer/text";
import { Occasion } from "./occasion";

/**
 * Represents the effect of a action in a battel.
 */
export class Effect<T = {}> {
  occasion: Occasion<T>;
  type: "damage" | "healing" | "bleed" | "confound" | "burn";
  factors: ("normal" | "fire")[] = [];
  value: Calculation;
  duration?: Calculation;

  constructor(occasion: Occasion<T>) {
    this.occasion = occasion;
    this.type = "damage";
    this.value = c();
  }

  /**
   * Make any dice roll for this Effect.
   */
  roll() {
    this.value.roll();
    if (this.duration !== undefined) {
      this.duration.roll();
    }
  }

  /**
   * Make a short text with infromation about the effect.
   *
   * @return a text information about the effect.
   */
  get showInfo(): Text {
    return t(
      this.occasion.action.showLabel,
      "⚔️ : ",
      t(this.value.showRange).span(6),
      "🎯: ",
      t(this.occasion.accuracy.result).percent,
      " ",
      "⏳: ",
      this.occasion.action.initiativ.showRange
    );
  }

  /**
   * Make a short text describing the effect.
   *
   * @return a text describing the effect.
   */
  get showDescription(): Text {
    return t(
      this.occasion.showTitel.green.newline,
      t().newline,
      this.occasion.action.description.newline,
      t().newline,
      t("⚔️ : ", this.value.toMimerText).newline,
      t("🎯: ", this.occasion.accuracy.toMimerText).percent.newline,
      t("⏳: ", this.occasion.action.initiativ.toMimerText).newline
    );
  }
}
