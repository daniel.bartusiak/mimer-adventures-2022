"use strict";
import { t, Text } from "@mimer/text";
import Mode from "./mode";
import Player from "./player";
import {Config} from "./game";

type startModeStatus = "active"|"end";

/**
 * The  Game class creting a game engine for logic.
 */
export default class Start extends Mode {
  status: startModeStatus = "active";

  constructor(players: Player[], config?:Config) {
    super(players, config);
  }

  move(playerId:number, command = {}):startModeStatus  {
    // Create message
    let result = t("🔥 Startar spel Mimer Äventyr 🔥 🤠\n").newline;

    // Send messages to all players
    for(const player of this.players) {
       player.msg.add(result);
    }

    return "end";
  }
}
