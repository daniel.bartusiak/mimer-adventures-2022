"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import { BattelAction } from "../../../game/battelAction";
import { Occasion } from "../../../game/occasion";
import Humanoid from "../../creatures/humanoids/humanoid";

export default function Knight(base: any) {
  return class Knight extends base {
    static creators = ["timce"];
    static value = 500;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost += Knight.value;
      this._description = t(
        "Riddaren är en mäktig krigare. ",
        "Ridaren gör stor skada genom att hugga med sitt ",
        "svärd. Rustningen skyddar mot olika attacker."
      );
      this._salute = t("Jag ska rädda dig ifrån detta hemska monster! 😇");
      this._lose = t("Ack och ve, onskan vinner igen i denna onda värld!");
      this._win = t("Rättvisan har segrat!");
      this._title = t("Riddare");
    }

    cut(): BattelAction {
      const my_action = new BattelAction(
        (this as unknown) as Humanoid,
        "Svärdshugg",
        "huggen med svärd"
      );
      const my_occation = my_action.createOccasion();
      const my_effect = my_occation.createEffect();

      my_action.tags.push("steel");
      my_action.initiativ.property("flexibility").add.value(2);
      my_action.description = t("Huggen med svärd");

      my_occation.accuracy.value(1).percent.value(90);

      my_effect.type = "damage";
      my_effect.value
        .dice(1, 10)
        .add.value(2)
        .mult.property("strength");

      return my_action;
    }

    selection(): BattelAction[] {
      let result = super.selection();
      result.push(this.cut());
      this.numberOfSelections++;
      return result;
    }

    damage(action: Occasion): Occasion {
      let result = super.damage(action);
      result.damage.sub.value(10);
      return result;
    }

    static info(): Text {
      return t("En bra krigare!").bold.blue;
    }
  };
}
