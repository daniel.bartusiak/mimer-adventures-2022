"use strict";
import { t } from "@mimer/text";
import Werewolf from "./werewolf";
import Human from "../human";

export default class Shapeshifterwerewolf extends Werewolf {
  static creators = ["miljonmannen"];
  static value = 400;

  constructor(name: string, type: string[], creators = Werewolf.creators) {
    super(name, ["Isig"].concat(type), creators);
    this.cost = this.cost + Werewolf.value;

    // Skapar en "exempel"- human för att dess konstruktor ska sätta upp egenskaperna efter human, och sedan använd dessa egenskaperna i skapandet av en människoliknande varulv.
    const originalHuman = new Human("Shapeshifter", []); // Ny funktionalitet som inte finns implementerad tidigare

    // Lägg till egenskaperna, intelligens & vishet från originalHuman.
    this.intelligent = originalHuman.intelligent;
    this.wisdom = originalHuman.wisdom;

    this._description = t(
      "En människa som kan förändras till varulv vid eget behov, därav har den kvar de mänskliga egenskaperna så som intellekt och kunskap."
    );
    this._title = t();
  }

  static infoChooseMe() {
    return t("Välj en intelligent varulv!");
  }

  static info() {
    return t("En människa som varulv!").blue;
  }
}
