"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction } from "../../../../game/battelAction";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Dwarf extends Humanoid {
  static creators = ["krm"];
  static value = 600;

  constructor(name: string, type: string[] = [], creators = Dwarf.creators) {
    super(name, ["Dvärg"].concat(type), creators);
    this.cost = this.cost + Dwarf.value;
    this.size = c()
      .value(4)
      .add.dice(0, 8);
    this.condition = c()
      .value(10)
      .add.dice(0, 10);
    this.strength = c(this)
      .value(10)
      .add.dice(0, 15);
    this.flexibility = c()
      .value(5)
      .add.dice(0, 10);
    this.intelligent = c()
      .value(10)
      .add.dice(0, 10);
    this.charisma = c()
      .value(5)
      .add.dice(0, 15);
    this.wisdom = c()
      .value(12)
      .add.dice(0, 10);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(85)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;
    this.cost += 600;

    this._description = t(
      "En liten varelse som ser ut som en människa och bor i hålor"
    );
    this._salute = t("Jag må vara liten men jag kan slåss");
    this._ask = t("Vad ska jag göra?");
    this._lose = t("Hur kan jag förlora?!?");
    this._win = t("Du skulle inte ha underskattat mig");
    this._title = t();
  }

  hardHit(): BattelAction {
    const my_action = new BattelAction(this, "Slår hårt", "hårt slagen");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t("Ett riktigt hårt slag!");

    my_effect.type = "damage";
    my_effect.value
      .dice(1, 10)
      .add.property("strength")
      .add.value(5);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.hardHit());
    this.numberOfSelections++;
    return result;
  }

  static infoChooseMe() {
    return t("Välj en dvärg");
  }

  static info() {
    return t("En vanlig Dvärg").blue;
  }
}
