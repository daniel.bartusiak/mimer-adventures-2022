# 0.2

- !Create a new projekt with out Old code.
- !fmt hela projektet med ny version.
- !Convert to new mimer-text version.
- !Redesign action.
- !Document action.
- !Implement some creature from old.
- !Update all classes in world with better typescript.
- !Implmenter simple run file.
- !Publish package

# 0.3

- !Fix any erros in the program.
- !change så aviw is used gram the mag tool.
- Writte a explanation of a battel.
- Writte an simplify version of above.
- Add documentation to battel code.
- Read code in game and make any simpel update to get action to work.
- Base aview on game engine, fixa errors by comment out.
- Fix all option in aview until "calculation".
- Fix option in aview "selection"
- Fix all option un aview until "damage"
- Fix option in aview "turn"
- Fix option in aview "battel"
- Fix option in aview "battelStats"

# 0.4 AI
- Review Game engine.
- Create an AI map.
- Implmentet RandomAI.
- Integrate AI to Game
- Implment ExpertAI(Using a simnpel json language).
- Add AI handlers to aview.

# 0.5

- Create a client code.
- Remove logger file and use a package instead.
- Create new server.
- Get it to work with selection mode.
- Create BattelGame for battel mode.
- Make server work with battelGame.
- Make the game playabel.
